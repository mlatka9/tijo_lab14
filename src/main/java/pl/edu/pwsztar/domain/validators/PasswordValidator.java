package pl.edu.pwsztar.domain.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {

    public static boolean isValid(final String password) {
        final int PASSWORD_MIN_LENGTH = 4;
        if(password == null || password.length() < PASSWORD_MIN_LENGTH) {
        return false;
    }



    String regex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])\\S{4,}";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(password);

    return matcher.matches();
    }
}
