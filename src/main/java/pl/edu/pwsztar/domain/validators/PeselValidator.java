package pl.edu.pwsztar.domain.validators;

import org.hibernate.validator.internal.constraintvalidators.hv.pl.PESELValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PeselValidator {

    public static boolean isValid(final String pesel) {
        final int LOGIN_MIN_LENGTH = 4;

        if(pesel == null || pesel.isEmpty()) {
            return false;
        }

        PESELValidator validator = new PESELValidator();
        validator.initialize(null);

        return validator.isValid(pesel, null);
    }

}
